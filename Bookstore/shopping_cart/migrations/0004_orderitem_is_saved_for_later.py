# Generated by Django 3.0.3 on 2020-04-02 02:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shopping_cart', '0003_auto_20200329_2311'),
    ]

    operations = [
        migrations.AddField(
            model_name='orderitem',
            name='is_saved_for_later',
            field=models.BooleanField(default=False),
        ),
    ]
